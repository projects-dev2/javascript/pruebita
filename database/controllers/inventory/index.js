"use strict";

module.exports = {

    // category
    listCategories: async (mysqlInstance) => {

        const query = "SELECT * FROM category";
        let process;

        try {
            process = await mysqlInstance.query(query, []);
        } catch (error) {
            console.log(error);
        }

        if (process !== undefined) {

            return {
                status: true,
                list: process,
            };

        } else {
            return {
                status: false,
                message: "No se listo los cursos del area",
            };
        }
    },

    infoCategory: async (mysqlInstance, idCategory) => {

        console.log('idCategory', idCategory);
        const query = `SELECT p.name, p.url_image, p.price, p.discount, c.name as name_categoria FROM product p INNER JOIN category c ON p.category = c.id WHERE c.id = ${idCategory}`;
        let process;

        try {
            process = await mysqlInstance.query(query, []);
        } catch (error) {
            console.log(error);
        }

        if (process !== undefined) {

            return {
                status: true,
                list: process,
            };

        } else {
            return {
                status: false,
                message: "No se listo los cursos del area",
            };
        }
    },

    // Product

    listProducts: async (mysqlInstance) => {

        const query = "SELECT p.name, p.url_image, p.price, p.discount, c.name as name_categoria FROM product p INNER JOIN category c ON p.category = c.id";
        let process;

        try {
            process = await mysqlInstance.query(query, []);
        } catch (error) {
            console.log(error);
        }

        if (process !== undefined) {
            return {
                status: true,
                list: process,
            };
        } else {
            return {
                status: false,
                message: "No se listo Datos de los productos",
            };
        }
    },

    // Search only product
    searchProdcut: async (mysqlInstance, product) => {

        const query = `SELECT * FROM product p WHERE p.name LIKE '${product}%'`;
        let process;

        try {
            process = await mysqlInstance.query(query, []);
            console.log(process);

        } catch (error) {
            console.log(error);
        }

        if (process !== undefined) {
            return {
                status: true,
                list: process,
            };
        } else {
            return {
                status: false,
                message: `No se encontro productos con el patron ${product}`,
            };
        }
    },

};