// Products
const listProducts = require('./products/listProducts');

// Categories
const listCategories = require('./categories/listCategories');
const infoCategory = require('./categories/infoCategory');

// Search - Filter
const searchProduct = require('./products/searchProduct');

module.exports = {
    listProducts,
    searchProduct,

    listCategories,
    infoCategory,
}
