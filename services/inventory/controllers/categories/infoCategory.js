'use strict'

const responseServerDataList = require('../../../../utils/responseServer/listItems');

module.exports = async (db, req, res) => {

    let { idCategory } = req.params;
    let listInfoCategory = {};
    let processStatus = true;

    try {
        listInfoCategory = await db.infoCategory(idCategory)
    } catch (error) {
        processStatus = false
    }

    const [statusCode, resp] = responseServerDataList(processStatus, listInfoCategory, 'cateogry', 'Data');
    res.status(statusCode).send({ resp })

}
