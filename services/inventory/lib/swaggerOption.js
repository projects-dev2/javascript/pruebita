const keys = require("../../../keys");

exports.optionSwagger = {
    definition:{
        openapi:"3.0.0",
        info:{
            title:'Desafio API',
            version:'1.0.0',
            description:'Documentacion de los endpoints'
        },
        servers:[
            {
                url:`http://localhost:${keys.ports_services.inventory}` 
            }
        ]
    },
    apis:["./services/inventory/routes/*.js"]

}